import { NgModule } from '@angular/core';
import { GameMainComponent } from './game-main/game-main';
import { GameMainChunkComponent } from './game-main-chunk/game-main-chunk';
@NgModule({
	declarations: [GameMainComponent,
    GameMainChunkComponent],
	imports: [],
	exports: [GameMainComponent,
    GameMainChunkComponent]
})
export class ComponentsModule {}
