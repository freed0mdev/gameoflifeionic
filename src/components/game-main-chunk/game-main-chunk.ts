import { Component } from '@angular/core';

/**
 * Generated class for the GameMainChunkComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-main-chunk',
  templateUrl: 'game-main-chunk.html'
})
export class GameMainChunkComponent {

  text: string;

  constructor() {
    console.log('Hello GameMainChunkComponent Component');
    this.text = 'Hello World';
  }

}
