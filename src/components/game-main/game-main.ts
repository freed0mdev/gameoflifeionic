import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { Platform } from 'ionic-angular';

/**
 * Generated class for the GameMainComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'game-main',
    templateUrl: 'game-main.html'
})
export class GameMainComponent implements OnInit {
    @ViewChild("chunkContainer", {read: ElementRef}) chunkContainer: ElementRef;

    currentStepPoints;
    diePoints;
    livePoints;
    playingInstance;
    worldWidth;
    worldHeight;

    constructor(public platform: Platform) {
        let worldWidth = platform.width() / 8 - 5;
        let worldHeight = platform.height() / 8 - 25;
        this.worldWidth = parseInt(worldWidth.toString());
        this.worldHeight = parseInt(worldHeight.toString());
        this.currentStepPoints = this.createMatrixArray(this.worldWidth, this.worldHeight);
    }

    ngOnInit() {
        this.renderChunks(this.currentStepPoints);
    }

    private createMatrixArray(rows, columns) {
        let arr = [];

        //generate array
        for (let y = 0; y < columns; y++) {
            arr[y] = [];
            for (let x = 0; x < rows; x++) {
                arr[y][x] = 0;
            }
        }

        return this.generateStartPosition(arr);
    }

    private generateStartPosition(arr) {
        let randomPointX = this.getRandomPointStart();
        let randomPointY = this.getRandomPointStart();

        this.generateAcorn(arr, randomPointX, randomPointY);

        return arr;
    }

    private generateAcorn(arr, randomX, randomY) {
        for (let y = 0; y < arr.length; y++) {
            for (let x = 0; x < arr[y].length; x++) {
                if (randomX == x && randomY == y) {
                    arr[y][x] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x - 2)] = 1;
                    arr[this.calculateStepOnGridY(y - 1)][this.calculateStepOnGridX(x - 2)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x - 3)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 1)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 2)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 3)] = 1;
                }
            }
        }
    }
Y
    private generateGlider(arr) {
        let randomPointX = this.getRandomPointStart();
        let randomPointY = this.getRandomPointStart();

        for (let y = 0; y < arr.length; y++) {
            for (let x = 0; x < arr[y].length; x++) {
                if (randomPointX == x && randomPointY == y) {
                    arr[y][x] = 1;
                    arr[this.calculateStepOnGridY(y)][this.calculateStepOnGridX(x)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 1)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 2)] = 1;
                    arr[this.calculateStepOnGridY(y)][this.calculateStepOnGridX(x + 2)] = 1;
                    arr[this.calculateStepOnGridY(y - 1)][this.calculateStepOnGridX(x + 2)] = 1;
                }
            }
        }
    }
    private generateLightWeightSpaceSheep(arr) {
        let randomPointX = this.getRandomPointStart();
        let randomPointY = this.getRandomPointStart();

        for (let y = 0; y < arr.length; y++) {
            for (let x = 0; x < arr[y].length; x++) {
                if (randomPointX == x && randomPointY == y) {
                    arr[y][x] = 1;
                    arr[this.calculateStepOnGridY(y)][this.calculateStepOnGridX(x + 3)] = 1;
                    arr[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x - 1)] = 1;
                    arr[this.calculateStepOnGridY(y + 2)][this.calculateStepOnGridX(x - 1)] = 1;
                    arr[this.calculateStepOnGridY(y + 2)][this.calculateStepOnGridX(x + 3)] = 1;
                    arr[this.calculateStepOnGridY(y + 3)][this.calculateStepOnGridX(x - 1)] = 1;
                    arr[this.calculateStepOnGridY(y + 3)][this.calculateStepOnGridX(x)] = 1;
                    arr[this.calculateStepOnGridY(y + 3)][this.calculateStepOnGridX(x + 1)] = 1;
                    arr[this.calculateStepOnGridY(y + 3)][this.calculateStepOnGridX(x + 2)] = 1;
                }
            }
        }
    }

    private getRandomPointStart() {
        return Math.floor((Math.random() * 10));
    }

    private pointsController() {
        this.diePoints = [];
        this.livePoints = [];

        for (let y = 0; y < this.currentStepPoints.length; y++) {
            for (let x = 0; x < this.currentStepPoints[y].length; x++) {
                let siblings = 0;

                if (this.currentStepPoints[y][x]) {
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y - 1)][this.calculateStepOnGridX(x - 1)];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y - 1)][x];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y - 1)][this.calculateStepOnGridX(x + 1)];
                    siblings += this.currentStepPoints[y][this.calculateStepOnGridX(x + 1)];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 1)];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y + 1)][x];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x - 1)];
                    siblings += this.currentStepPoints[y][this.calculateStepOnGridX(x - 1)];
                    if (!(siblings >= 2 && siblings <= 3)) {
                        this.diePoints.push([y, x]);
                    }
                } else {
                    //new life
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y - 1)][this.calculateStepOnGridX(x - 1)];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y - 1)][x];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y - 1)][this.calculateStepOnGridX(x + 1)];
                    siblings += this.currentStepPoints[y][this.calculateStepOnGridX(x + 1)];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x + 1)];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y + 1)][x];
                    siblings += this.currentStepPoints[this.calculateStepOnGridY(y + 1)][this.calculateStepOnGridX(x - 1)];
                    siblings += this.currentStepPoints[y][this.calculateStepOnGridX(x - 1)];
                    if (siblings == 3) {
                        this.livePoints.push([y, x]);
                    }
                }
            }
        }

        this.diePoints.map((coords) => {
            this.setPointValue(coords[0], coords[1], 0);
        });

        this.livePoints.map((coords) => {
            this.setPointValue(coords[0], coords[1], 1);
        });

        this.diePoints = [];
        this.livePoints = [];

        let randomGeneration = Math.random() * 200;

        if (randomGeneration > 190) {
            this.generateGlider(this.currentStepPoints);
        } else if (randomGeneration == 200) {
            this.generateLightWeightSpaceSheep(this.currentStepPoints);
        }

    }

    private calculateStepOnGridX(sum) {
        sum < 0 ? sum += this.worldWidth : sum > this.worldWidth - 1 ? sum -= this.worldWidth : false;

        return sum;
    }
    private calculateStepOnGridY(sum) {
        sum < 0 ? sum += this.worldHeight : sum > this.worldHeight - 1 ? sum -= this.worldHeight : false;

        return sum;
    }

    play() {
        if (!this.playingInstance) {
            this.playingInstance = setInterval(() =>
                this.pointsController(), 100);
        }
    }

    stop() {
        clearInterval(this.playingInstance);
        this.playingInstance = null;
    }

    clear() {
        for (let y = 0; y < this.currentStepPoints.length; y++) {
            for (let x = 0; x < this.currentStepPoints[y].length; x++) {
                this.setPointValue(y, x, 0);
            }
        }
    }

    private setPointValue(coordY, coordX, value) {
        let chunk = document.querySelector(`.js-chunk[data-coords="${coordY},${coordX}"]`);
        chunk.setAttribute('data-index', `${value}`);
        this.currentStepPoints[coordY][coordX] = value;
    }

    public setPoints(elem) {
        this.stop();
        let chunkCoords = elem.dataset.coords.split(',');
        this.setPointValue(chunkCoords[0], chunkCoords[1], 1);
    }

    private renderChunks(array) {
        let html = '';

        array.map((arr, indexY) => {
            arr.map((arr, indexX) => {
                html += `<div class="Chunk js-chunk"
                        data-coords="${indexY},${indexX}"  onclick="life.setPoints(this);" data-index="${arr}"></div>`;
            });
        });

        this.chunkContainer.nativeElement.innerHTML = html;
        this.chunkContainer.nativeElement.style.width = this.worldWidth * 8 + "px";
    }
}
